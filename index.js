#!/usr/bin/env node
'use strict';

const arrify = require('arrify');
const meow = require('meow');
const getStdin = require('get-stdin');
const imagemin = require('imagemin');
const ora = require('ora');
const plur = require('plur');
const stripIndent = require('strip-indent');

const cli = meow(`
Uso
$ img-compressor <path|glob> ... --out-dir=dest [--plugin=<name> ...]
$ img-compressor <arquivo> > <arquivo-min>

Opcoes
-p, --plugin   Sobrescreve os plugins default
-o, --out-dir  diretorio de destino

Examples
$ img-compressor images/* --out-dir=build
$ img-compressor imagem.png > imagem-min.png
$ img-compressor --plugin=svgo icone.svg > icone-min.svg
`, {
  string: [
    'plugin',
    'out-dir'
  ],
  alias: {
    p: 'plugin',
    o: 'out-dir'
  }
});

const DEFAULT_PLUGINS = [
  'gifsicle',
  'jpegtran',
  'optipng',
  'svgo'
];

const requirePlugins = plugins => plugins.map(x => {
  try {
    return require(`imagemin-${x}`)();
  } catch (err) {
    console.error(stripIndent(`
    Plugin desconhecido: ${x}
    O plugin foi instalado?
    Para instalar:
    $ npm install -g imagemin-${x}
		`).trim());
    process.exit(1);
  }
});

const run = (input, opts) => {
  opts = Object.assign({ plugin: DEFAULT_PLUGINS }, opts);
  
  const use = requirePlugins(arrify(opts.plugin));
  const spinner = ora('Comprimindo imagens');
  
  if (Buffer.isBuffer(input)) {
    imagemin.buffer(input, { use }).then(buf => process.stdout.write(buf));
    return;
  }
  
  if (opts.outDir) {
    spinner.start();
  }
  
  imagemin(input, opts.outDir, { use })
  .then(files => {
    if (!opts.outDir && files.length === 0) {
      return;
    }
    
    if (!opts.outDir && files.length > 1) {
      console.error('Escolha um diretório de destino `--out-dir`');
      process.exit(1);
    }
    
    if (!opts.outDir) {
      process.stdout.write(files[0].data);
      return;
    }
    
    spinner.stop();
    
    console.log(`${files.length} ${plur('image', files.length)} comprimido`);
  })
  .catch(err => {
    spinner.stop();
    throw err;
  });
};

if (cli.input.length === 0 && process.stdin.isTTY) {
  console.error('Digite no mínimo um nome de arquivo');
  process.exit(1);
}

if (cli.input.length > 0) {
  run(cli.input, cli.flags);
} else {
  getStdin.buffer().then(buf => run(buf, cli.flags));
}
